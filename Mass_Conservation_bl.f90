MODULE MASS_CONSERVATION_MODULE


  USE THICKNESS_MODULE
  USE JACOBI_THICKNESS_MODULE
  USE NONA_DIAG
  USE PRESSURE

CONTAINS


  SUBROUTINE  MASS_CONSERVATION(H,P,T,BL,Dt,Dr,Mtot,dist,ray,el,grav,sigma,nu,delta0)


    !****Initialisation******

    IMPLICIT NONE

    DOUBLE PRECISION  , DIMENSION(:,:), INTENT(INOUT)         :: H,P,T,BL
    DOUBLE PRECISION  ,                 INTENT(IN)            :: Dt,Dr
    DOUBLE PRECISION  , DIMENSION(:),   INTENT(IN)            :: dist,ray
    DOUBLE PRECISION  ,                 INTENT(IN)            :: el,sigma,grav,nu,delta0
    INTEGER,                            INTENT(IN)            :: Mtot

    DOUBLE PRECISION                                          :: theta,Ite,U
    DOUBLE PRECISION ,DIMENSION(:,:),ALLOCATABLE              :: Coeff
    INTEGER                                                   :: ndyke,i,Tmax,N,algo1
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: fguess,ftmps
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: a,b,c,d,e,f,g,k,l,S
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: a1,b1,c1,d1,e1,f1,g1,k1,l1
    INTEGER                                                   :: err1,err2,col
    DOUBLE PRECISION  ,DIMENSION(:), ALLOCATABLE              :: Hm,qa
    LOGICAL                                                   :: cho

    theta=1d0 ! Facteur qui détermine le shéma utilisé
    Ite=1! Facteur qui permet de ralentir ou accelerer l'itération

    ndyke=sigma/Dr
    CHO=COUNT(H(:,1)>0.d0)<ndyke
    SELECT CASE (CHO)
    CASE(.TRUE.)
       Tmax=ndyke+3    ! Cas ou on donne pas de profile initiale...
    CASE(.FALSE.)
       Tmax=COUNT(H(:,1)>0.d0)
    END SELECT
    N=Tmax


    !! REMPLISSAGE DU FLUX

    ALLOCATE(qa(1:N),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    DO i=1,N,1
       U=2.d0/(sigma)**4.
       IF (i<ndyke+1) THEN
          qa(i)=U*(sigma**2.-dist(i)**2.)
       ELSE
          qa(i)=0.d0
       END IF
    END DO

    !! CALCUL DES COEFFICIENT DE LA PRESSION

    ALLOCATE(Coeff(1:N,7),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    algo1=1;col=1
    CALL PRESSION_ORDRE4(Mtot,N,Dr,P,H,dist,Coeff,algo1,col,el,delta0)


    !! REMPLISSAGE DU MEMBRE GRAVI POUR LE TERME H AU TEMPS n et au guess
    ALLOCATE(ftmps(1:N),fguess(1:N),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    col=1
    CALL THICKNESS(ftmps,col,N,Mtot,H,P,T,BL,Coeff,Dt,Dr,dist,ray,qa,el,grav,nu,delta0)
    col=2
    CALL THICKNESS(fguess,col,N,Mtot,H,P,T,BL,Coeff,Dt,Dr,dist,ray,qa,el,grav,nu,delta0)

  
!!! REMPLISSAGE DE LA MATRICE JACOBIENNE

    ALLOCATE(a1(1:N),b1(1:N),c1(1:N),d1(1:N),e1(1:N),stat=err1)
    ALLOCATE(f1(1:N),g1(1:N),k1(1:N),l1(1:N),stat=err2)
    IF (err1>1 .OR. err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    CALL JACOBI_THICKNESS(a1,b1,c1,d1,e1,f1,g1,k1,l1,N,H,P,T,BL,Coeff,Dt,Dr,dist,ray,el,grav,nu,Mtot,delta0)
 
!!! REMPLISSAGE DU SYSTEME A INVERSER

    ALLOCATE(a(1:N),b(1:N),c(1:N),d(1:N),e(1:N),stat=err1)
    ALLOCATE(f(1:N),g(1:N),k(1:N),l(1:N),S(1:N),stat=err2)
    IF (err1>1 .OR. err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    DO i=1,N,1
       a(i)=-theta*Dt*a1(i)
       b(i)=-theta*Dt*b1(i)
       c(i)=-theta*Dt*c1(i)
       d(i)=-theta*Dt*d1(i)
       e(i)=1.d0-theta*Dt*e1(i)
       f(i)=-theta*Dt*f1(i)
       g(i)=-theta*Dt*g1(i)
       k(i)=-theta*Dt*k1(i)
       l(i)=-theta*Dt*l1(i)
       S(i)=H(i,1)-H(i,2)+theta*Dt*fguess(i)+(1-theta)*Dt*ftmps(i)
    END DO

    a(1)=0.d0;a(2)=0.d0;a(3)=0.d0;a(4)=0.d0
    b(1)=0.d0;b(2)=0.d0;b(3)=0.d0
    c(1)=0.d0;c(2)=0.d0
    d(1)=0.d0
    l(N-3)=0.d0;l(N-2)=0.d0;l(N-1)=0.d0;l(N)=0.d0
    k(N-2)=0.d0;k(N-1)=0.d0;k(N)=0.d0
    g(N-1)=0.d0;g(N)=0.d0
    f(N)=0.d0

!!!INVERSION DE LA MATRICE ET SORTI DE L'ERREUR

    ALLOCATE(Hm(1:N),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans vecteur Hm'; STOP
    END IF

    CALL NONA_DIAGO(N,Hm,a,b,c,d,e,f,g,k,l,S)

!!! INCORXORATION DU Hm DANS UNE MATRICE DE TAILLE Mtot


    DO i=1,N,1
       H(i,3)=Hm(i)+H(i,2)
       H(i,4)=Hm(i)
    END DO

    H(N+1:Mtot,3)=0.d0
    H(N+1:Mtot,4)=0.d0

    !! CALCULE DE LA PRESSION

    algo1=2;col=3
    CALL PRESSION_ORDRE4(Mtot,N,Dr,P,H,dist,Coeff,algo1,col,el,delta0)

    DEALLOCATE(Hm,a,b,c,d,e,f,g,k,l,S,Coeff,qa)
    DEALLOCATE(fguess,ftmps,a1,b1,c1,d1,e1,f1,g1,k1,l1)

  END SUBROUTINE MASS_CONSERVATION

END MODULE MASS_CONSERVATION_MODULE

