MODULE TEMPERATURE_MODULE

CONTAINS

  SUBROUTINE  TEMPERATURE(Te,col,N,Xi,H,T,BL,P,dist,ray,Dr,nu,Phi,qa,delta0,el,grav)


    !### INTIALISATION DES VARIABLES ###!

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)          :: Te
    INTEGER           ,INTENT(IN)                             :: col,N
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: Xi,H,T,BL,P
    DOUBLE PRECISION  ,INTENT(IN)                             :: Dr 
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: qa,dist,ray
    DOUBLE PRECISION  ,INTENT(IN)                             :: nu,Phi,delta0,el,grav

    DOUBLE PRECISION                                          :: Ai,Bi
    DOUBLE PRECISION                                          :: h_a,h_a2,h_a3,delta_a,delta_a2,eta_a,zeta_a,Omega_a,T_a
    DOUBLE PRECISION                                          :: Psi_a,Sigma_a
    DOUBLE PRECISION                                          :: h_b,h_b2,h_b3,delta_b,delta_b2,eta_b,zeta_b,Omega_b,T_b
    DOUBLE PRECISION                                          :: Psi_b,Sigma_b
    DOUBLE PRECISION                                          :: loss
    INTEGER                                                   :: i


    !### REMPLISSAGE DE T ###!

    DO i=1,N,1
 
       IF1: IF (i/=1 .AND. i/=N) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3));h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2);h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          h_a3=0.5d0*(H(i+1,3)**3+H(i,3)**3);h_b3=0.5d0*(H(i,3)**3+H(i-1,3)**3)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col));delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2);delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_a=(grav*(H(i+1,3)-H(i,3))+el*(P(i+1,3)-P(i,3)))/Dr
          eta_b=(grav*(H(i,3)-H(i-1,3))+el*(P(i,3)-P(i-1,3)))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col));T_b=0.5d0*(T(i,col)+T(i-1,col))
          zeta_a=h_a*(sqrt(5.d0)*h_a+sqrt(2.d0)*delta_a)/(sqrt(2.d0))
          zeta_b=h_b*(sqrt(5.d0)*h_b+sqrt(2.d0)*delta_b)/(sqrt(2.d0))
          Psi_a=(delta_a2/3.d0)*(2*h_a+(8.d0/35.d0)*delta_a)-2*h_a3
          Psi_b=(delta_b2/3.d0)*(2*h_b+(8.d0/35.d0)*delta_b)-2*h_b3
          Sigma_a=eta_a*(((sqrt(10.d0)-3.d0)/5.d0)*nu*h_a*zeta_a+(1.d0-nu)*T_a*Psi_a)*T_a
          Sigma_b=eta_b*(((sqrt(10.d0)-3.d0)/5.d0)*nu*h_b*zeta_b+(1.d0-nu)*T_b*Psi_b)*T_b
          Omega_a=eta_a*((3.d0)/(5.d0)*nu*(zeta_a-delta_a2)+(1.d0-nu)*T_a*(3*h_a2+delta_a2))
          Omega_b=eta_b*((3.d0)/(5.d0)*nu*(zeta_b-delta_b2)+(1.d0-nu)*T_b*(3*h_b2+delta_b2))
       ELSE IF (i==N) THEN
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          h_b3=0.5d0*(H(i,3)**3+H(i-1,3)**3)
          delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_b=(grav*(H(i,3)-H(i-1,3))+el*(P(i,3)-P(i-1,3)))/Dr
          T_b=0.5d0*(T(i,col)+T(i-1,col))
          zeta_b=h_b*(sqrt(5.d0)*h_b+sqrt(2.d0)*delta_b)/(sqrt(2.d0))
          Psi_b=(delta_b2/3.d0)*(2*h_b+(8.d0/35.d0)*delta_b)-2*h_b3
          Sigma_b=eta_b*(((sqrt(10.d0)-3.d0)/5.d0)*nu*h_b*zeta_b+(1.d0-nu)*T_b*Psi_b)*T_b
          Omega_b=eta_b*((3.d0)/(5.d0)*nu*(zeta_b-delta_b2)+(1.d0-nu)*T_b*(3*h_b2+delta_b2))
       ELSE IF (i==1) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2)
          h_a3=0.5d0*(H(i+1,3)**3+H(i,3)**3)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2)
          eta_a=(grav*(H(i+1,3)-H(i,3))+el*(P(i+1,3)-P(i,3)))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col))
          zeta_a=h_a*(sqrt(5.d0)*h_a+sqrt(2.d0)*delta_a)/(sqrt(2.d0))
          Psi_a=(delta_a2/3.d0)*(2*h_a+(8.d0/35.d0)*delta_a)-2*h_a3
          Sigma_a=eta_a*(((sqrt(10.d0)-3.d0)/5.d0)*nu*h_a*zeta_a+(1.d0-nu)*T_a*Psi_a)*T_a
          Omega_a=eta_a*((3.d0)/(5.d0)*nu*(zeta_a-delta_a2)+(1.d0-nu)*T_a*(3*h_a2+delta_a2))
             
       END IF IF1
       
       IF2: IF (BL(i,col)==0.d0) THEN
          loss=0.d0
       ELSE
          loss=(4*Phi*T(i,col))/BL(i,col)
       END IF IF2

       IF3: IF (i==1) THEN
          Te(i)=qa(i)-loss+Ai*Omega_a*Xi(i,col)+Ai*Sigma_a
       ELSEIF (i==N) THEN
          Te(i)=qa(i)-loss-Bi*Omega_b*Xi(i-1,col)-Bi*Sigma_b
        ELSE
           Te(i)=Ai*Omega_a*Xi(i,col)-Bi*Omega_b*Xi(i-1,col)&
               &+Ai*Sigma_a-Bi*Sigma_b &
               &-loss+qa(i)
          ! print*,i,(4*Phi*T(i,col))/BL(i,col)
        END IF IF3

    END DO

  END SUBROUTINE TEMPERATURE

END MODULE TEMPERATURE_MODULE

