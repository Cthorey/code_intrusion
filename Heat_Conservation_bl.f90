MODULE HEAT_CONSERVATION_MODULE


  USE TEMPERATURE_MODULE
  USE JACOBI_TEMPERATURE_MODULE
  USE NONA_DIAG
  USE XI_SPLIT

CONTAINS

  SUBROUTINE  HEAT_CONSERVATION(Xi,H,P,T,BL,Dt,Dr,dist,ray,Mtot,sigma,nu,Phi,delta0,el,grav)


    !****Initialisation******


    IMPLICIT NONE

    DOUBLE PRECISION  , DIMENSION(:,:), INTENT(INOUT)         :: Xi,H,P,T,BL
    DOUBLE PRECISION  ,                 INTENT(IN)            :: Dt,Dr
    DOUBLE PRECISION  , DIMENSION(:),   INTENT(IN)            :: dist,ray
    DOUBLE PRECISION  ,                 INTENT(IN)            :: sigma,nu,Phi,delta0,el,grav
    INTEGER,                            INTENT(IN)            :: Mtot

    DOUBLE PRECISION                                          :: theta,Ite,U
    INTEGER                                                   :: ndyke,i,Tmax,N
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: Xi_guess,Xi_tmps
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: a,b,c,d,e,f,g,k,l,S
    DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE                :: a1,b1,c1,d1,e1,f1,g1,k1,l1
    INTEGER                                                   :: err1,err2,col
    DOUBLE PRECISION  ,DIMENSION(:), ALLOCATABLE              :: Xi_m,qa
    LOGICAL                                                   :: cho


    theta=1d0 ! Facteur qui détermine le shéma utilisé
    Ite=1! Facteur qui permet de ralentir ou accelerer l'itération

    ndyke=sigma/Dr
    CHO=COUNT(H(:,1)>0.d0)<ndyke
    SELECT CASE (CHO)
    CASE(.TRUE.)
       Tmax=ndyke    ! Cas ou on donne pas de profile initiale...
    CASE(.FALSE.)
       Tmax=COUNT(H(:,1)>0.d0)
    END SELECT

    N=Tmax

    !! REMPLISSAGE DU FLUX

    ALLOCATE(qa(1:N+1),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF
    DO i=1,N,1
       U=2.d0/(sigma)**4
       IF (i<ndyke+1) THEN
          qa(i)=U*(sigma**2-dist(i)**2)
       ELSE
          qa(i)=0.d0
       END IF
    END DO

    !! REMPLISSAGE DE T AU TEMPS t ET GUESS

    ALLOCATE(Xi_tmps(1:N),Xi_guess(1:N),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    col=1
    CALL TEMPERATURE(Xi_tmps,col,N,Xi,H,T,BL,P,dist,ray,Dr,nu,Phi,qa,delta0,el,grav)
    col=2
    CALL TEMPERATURE(Xi_guess,col,N,Xi,H,T,BL,P,dist,ray,Dr,nu,Phi,qa,delta0,el,grav)

    !! REMPLISSAGE DE LA MATRICE JACOBIENNE

    ALLOCATE(a1(1:N),b1(1:N),c1(1:N),d1(1:N),e1(1:N),stat=err1)
    ALLOCATE(f1(1:N),g1(1:N),k1(1:N),l1(1:N),stat=err2)
    IF (err1>1 .OR. err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    CALL JACOBI_TEMPERATURE(a1,b1,c1,d1,e1,f1,g1,k1,l1,N,H,T,P,BL,Dr,dist,ray,nu,Phi,qa,delta0,el,grav)

    !! REMPLISSAGE DU SYSTEME A INVERSER

    ALLOCATE(a(1:N),b(1:N),c(1:N),d(1:N),e(1:N),stat=err1)
    ALLOCATE(f(1:N),g(1:N),k(1:N),l(1:N),S(1:N),stat=err2)
    IF (err1>1 .OR. err2>1) THEN
       PRINT*, 'Erreur d''allocation dans coeff du systeme'; STOP
    END IF

    DO i=1,N,1
       a(i)=-theta*Dt*a1(i)
       b(i)=-theta*Dt*b1(i)
       c(i)=-theta*Dt*c1(i)
       d(i)=-theta*Dt*d1(i)
       e(i)=1.d0-theta*Dt*e1(i)
       f(i)=-theta*Dt*f1(i)
       g(i)=-theta*Dt*g1(i)
       k(i)=-theta*Dt*k1(i)
       l(i)=-theta*Dt*l1(i)
       S(i)=Xi(i,1)-Xi(i,2)+theta*Dt*Xi_guess(i)+(1-theta)*Dt*Xi_tmps(i)
    END DO


    a(1)=0;a(2)=0;a(3)=0;a(4)=0
    b(1)=0;b(2)=0;b(3)=0
    c(1)=0;c(2)=0
    d(1)=0
    l(N-3)=0;l(N-2)=0;l(N-1)=0;l(N)=0
    k(N-2)=0;k(N-1)=0;k(N)=0
    g(N-1)=0;g(N)=0
    f(N)=0

    !!INVERSION DE LA MATRICE ET SORTI DE L'ERREUR

    ALLOCATE(Xi_m(1:N),stat=err2)
    IF (err2>1) THEN
       PRINT*, 'Erreur d''allocation dans vecteur Hm'; STOP
    END IF

    CALL NONA_DIAGO(N,Xi_m,a,b,c,d,e,f,g,k,l,S)

    DO i=1,N,1
       IF (ABS(Xi_m(i))<1D-10) THEN
         ! Xi_m(i)=0.d0
       END IF
       Xi(i,3)=Xi_m(i)+Xi(i,2)
       Xi(i,4)=Xi_m(i)
    END DO

    
    !! UPDATE DES VARIABLE T ET BL

    CALL Xi_Splitting(Xi,H,BL,T,N)


    DEALLOCATE(Xi_m,qa,a,b,c,d,e,f,g,k,l,S)
    DEALLOCATE(Xi_guess,Xi_tmps,a1,b1,c1,d1,e1,f1,g1,k1,l1)

  END SUBROUTINE HEAT_CONSERVATION

END MODULE HEAT_CONSERVATION_MODULE
