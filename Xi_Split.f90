MODULE XI_SPLIT

CONTAINS

  SUBROUTINE Xi_Splitting(Xi,H,BL,T,N)

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:,:)  , INTENT(IN)             :: H
    DOUBLE PRECISION  ,DIMENSION(:,:)  , INTENT(INOUT)          :: T,BL,Xi
    INTEGER           ,INTENT(IN)                               :: N

    INTEGER                                                     :: i

!!! Test sur XI 
    DO i=1,N
       IF (Xi(i,3)>H(i,3) ) THEN
          print*,'erreur ds Xi',i,Xi(i,3),H(i,3)
         !! Xi(i,3)=H(i,3)
       END IF
    END DO

!!! RECONSTRUCTION DU VECTEURS COUCHE LIMITES

    DO i=1,N
       IF ((2.d0*H(i,3))/(3.d0)< Xi(i,3) .AND. Xi(i,3)<H(i,3)) THEN
          BL(i,3)=((3.d0)/(2.d0))*(H(i,3)-Xi(i,3))
          T(i,3)=1
       ELSEIF (Xi(i,3)<(2.d0/(3.d0))*H(i,3)) THEN
          BL(i,3)=H(i,3)/2
          T(i,3)=(3.d0*Xi(i,3))/(2*H(i,3))
       END IF
    END DO


  END SUBROUTINE Xi_Splitting

END MODULE Xi_Split
