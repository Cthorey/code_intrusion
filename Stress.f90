MODULE STRESS_FIELD

CONTAINS

SUBROUTINE  STRESS_ELASTIC_FIELD(Srr,Stt,H,dist,Dr,Mtot)

IMPLICIT NONE

DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: H
DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: dist
DOUBLE PRECISION  ,INTENT(IN)                             :: Dr
INTEGER           ,INTENT(IN)                             :: Mtot

DOUBLE PRECISION  ,DIMENSION(:),   INTENT(INOUT)          :: Srr,Stt

INTEGER                                                   :: i
DOUBLE PRECISION                                          :: nuu

nuu=0.25

DO i=1,Mtot-2
             IF (i==1) THEN
                Srr(i)=-(1.d0/(12*Dr**2*dist(i)))*((nuu*Dr-dist(i))*H(i+1,3)+(16*dist(i)-8*nuu*Dr)*H(i,3)&
                     &-30*dist(i)*H(i,3)+(16*dist(i)+8*nuu*Dr)*H(i+1,3)-(dist(i)+nuu*Dr)*H(i+2,3))
                Stt(i)=-(1.d0/(12*Dr**2*dist(i)))*((Dr-nuu*dist(i))*H(i+1,3)+(16*nuu*dist(i)-8*Dr)*H(i,3)&
                     &-30*nuu*dist(i)*H(i,3)+(16*nuu*dist(i)+8*Dr)*H(i+1,3)-(Dr+nuu*dist(i))*H(i+2,3))
             ELSEIF (i==2) THEN
                Srr(i)=-(1.d0/(12*Dr**2*dist(i)))*((nuu*Dr-dist(i))*H(i-1,3)+(16*dist(i)-8*nuu*Dr)*H(i-1,3)&
                     &-30*dist(i)*H(i,3)+(16*dist(i)+8*nuu*Dr)*H(i+1,3)-(dist(i)+nuu*Dr)*H(i+2,3))
                Stt(i)=-(1.d0/(12*Dr**2*dist(i)))*((Dr-nuu*dist(i))*H(i-1,3)+(16*nuu*dist(i)-8*Dr)*H(i-1,3)&
&-30*nuu*dist(i)*H(i,3)+(16*nuu*dist(i)+8*Dr)*H(i+1,3)-(Dr+nuu*dist(i))*H(i+2,3))
             ELSE
                Srr(i)=-(1.d0/(12*Dr**2*dist(i)))*((nuu*Dr-dist(i))*H(i-2,3)+(16*dist(i)-8*nuu*Dr)*H(i-1,3)&
&-30*dist(i)*H(i,3)+(16*dist(i)+8*nuu*Dr)*H(i+1,3)-(dist(i)+nuu*Dr)*H(i+2,3))
                Stt(i)=-(1.d0/(12*Dr**2*dist(i)))*((Dr-nuu*dist(i))*H(i-2,3)+(16*nuu*dist(i)-8*Dr)*H(i-1,3)&
&-30*nuu*dist(i)*H(i,3)+(16*nuu*dist(i)+8*Dr)*H(i+1,3)-(Dr+nuu*dist(i))*H(i+2,3))          
             END IF
          END DO

        END SUBROUTINE STRESS_ELASTIC_FIELD

      END MODULE STRESS_FIELD
