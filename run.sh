#!/bin/bash
gfortran -c constante_tmp.f90 Xi_Split.f90
gfortran -c Module_Output.f90 Module_Input.f90
gfortran -c Pressure.f90 IsNumber.f90 Module_Initialisation.f90 
gfortran -c Thickness_bl.f90 Nona.f90 Jacobian_thickness_bl.f90 
gfortran -c Mass_Conservation_bl.f90 Stress.f90
gfortran -c Temperature_bl.f90 Jacobian_Temperature_bl.f90
gfortran -c Heat_Conservation_bl.f90 constante_tmp.f90
gfortran -c Module_Epaisseur.f90 Module_Chaleur.f90
gfortran main.f90 Pressure.o Thickness_bl.o Nona.o Jacobian_thickness_bl.o Mass_Conservation_bl.o Temperature_bl.o Jacobian_Temperature_bl.o Heat_Conservation_bl.o constante_tmp.o Stress.o IsNumber.o Module_Initialisation.o Module_Output.o Module_Input.o Module_Epaisseur.o Module_Chaleur.o Xi_Split.o -o run
rm *.o
rm *.mod
