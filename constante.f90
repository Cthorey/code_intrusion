MODULE CONSTANTE

 
CONTAINS

  SUBROUTINE CONST(Mtot,Te,Dt,Dr,el,grav,sigma,nu,Phi,sample,delta0,&
       &epsilon,epsilon2,Format_O,Format_NSD,Init,Input_Racine,Output_Racine,Input_Data_Name&
       &,Format_NSD_Init_0,Format_NSD_Init_1,Format_Input_Data,Format_RV,Format_Backup)

    IMPLICIT NONE

    DOUBLE PRECISION                           , INTENT(OUT)                   :: Te
    INTEGER                                    , INTENT(OUT)                   :: Mtot,sample,Init
    DOUBLE PRECISION                           , INTENT(OUT)                   :: el,grav,sigma,nu,Phi,delta0 !Nombre sans dimension
    DOUBLE PRECISION                           , INTENT(OUT)                   :: Dt,Dr,epsilon,epsilon2

    CHARACTER(LEN=300)                         , INTENT(INOUT)                 :: Output_Racine,Format_O,Format_NSD
    CHARACTER(LEN=300)                         , INTENT(INOUT)                 :: Input_Racine,Input_Data_Name
    CHARACTER(LEN=300)                         , INTENT(INOUT)                 :: Format_NSD_Init_0,Format_NSD_Init_1
    CHARACTER(LEN=300)                         , INTENT(INOUT)                 :: Format_Input_Data,Format_RV,Format_Backup
    
    CHARACTER(LEN=300)                                                         :: Output_Name_NSD,Format_Racine
    CHARACTER(LEN=8)                                                           :: sigma_s,delta0_s,nu_s,Phi_s,Dr_s,Dt_s,epsilon_s
    CHARACTER(LEN=3)                                                           :: I_Racine,O_Racine
    CHARACTER(LEN=2)                                                           :: I_D_R,NSD

    CHARACTER(LEN=Size):: Root

!!! PARAMETRE DE LA GRILLE

    Te = 1D32  
    Mtot = 2000     
    Dt = 1D-5
    Dr = 1D-2
    epsilon = 1D-5
    epsilon2 = 1D-3

!!! NOMBRE SANS DIMENSION

    Sigma = Null
    Delta0 = Null
    Grav = Null
    El = Null
    Nu = Null
    Phi = Null
    sample = (Dt)/(Dt)
    Init = Null
    Input_Data_Name = Null
    Root = Null
    Input_Racine = Root//Null
    Output_Racine = Root//Null
 
! FINIR PAR UN /

    write(I_Racine,'(I3)'),len(trim(Input_Racine))
    write(O_Racine,'(I3)'),len(trim(Output_Racine))
    write(I_D_R,'(I2)'),len(trim(Input_Data_Name))
    write(NSD,'(I2)'),len(trim('NbSsDim.txt'))

    Format_NSD_Init_1='(a'//I_Racine//',a'//NSD//')'
    Format_Input_Data='(a'//I_Racine//',a'//I_D_R//')'
    Format_NSD_Init_0='(a'//I_Racine//',a'//NSD//')'
    Format_RV='(a'//O_Racine//',a3,i7.7,a4)'
    Format_Backup='(a'//O_Racine//',a7,i7.7,a4)'
    

!!! FORMAT ECRITURE FICHIER SORTIE

    Format_O ="(I10,2X,I10,2X,I10,2X,I10,2X,I5,2X,I10,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,&
         &2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,D20.14,2X,D20.14,2X,D20.14&
         &,D20.14,2X,D20.14,2X,D20.14)"

!!! ECRITURE DU FICHIER POUR RANGER LES NOMBRES SANS DIMENSIONS

    Format_NSD = "(D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,&
         &2X,D20.14,2X,D20.14,2X,D20.14,2X,D20.14,2X,I10,2X,I10)"

    IF (Init==0) THEN
       WRITE(Output_Name_NSD,Format_NSD_Init_0),Output_Racine,'NbSsDim.txt'
       OPEN(unit=1,file=Output_Name_NSD,status='replace')
       WRITE(1,Format_NSD),sigma,el,grav,delta0,nu,Phi,Dt,Dr,epsilon,epsilon2,Mtot,sample
       CLOSE(1)
    END IF

!!! NOM DU FICHIER OUTPUT CREER

    WRITE(sigma_s,'(E8.1)') sigma;WRITE(delta0_s,'(E8.1)') delta0
    WRITE(nu_s,'(E8.1)') nu;WRITE(Phi_s,'(E8.1)') Phi
    WRITE(Dr_s,'(E8.1)') Dr;WRITE(Dt_s,'(E8.1)') Dt
    WRITE(epsilon_s,'(E8.1)') epsilon
    sigma_s=TRIM(ADJUSTL(sigma_s(6:8)));delta0_s=TRIM(ADJUSTL(delta0_s(6:8)))
    nu_s=TRIM(ADJUSTL(nu_s(6:8)));Phi_s=TRIM(ADJUSTL(Phi_s(6:8)))
    Dr_s=TRIM(ADJUSTL(Dr_s(6:8)));Dt_s=TRIM(ADJUSTL(Dt_s(6:8)))
    epsilon_s=TRIM(ADJUSTL(epsilon_s(6:8)))
   ! WRITE(Output_name,'(a8,a3,a2,a3,a2,a3,a2,a3,a2,a3,a2,a3,a2,a3)') 'Output_D',delta0_s,'_S',sigma_s,'_N',nu_s,'_P',Phi_s&
   !      &,'_Dr',Dr_s,'_Dr',Dt_s,'_E',epsilon_s


  END SUBROUTINE CONST
END MODULE CONSTANTE


