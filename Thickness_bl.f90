MODULE THICKNESS_MODULE


  USE PRESSURE

CONTAINS

  SUBROUTINE  THICKNESS(f,col,N,Mtot,H,P,T,BL,Coeff,Dt,Dr,dist,ray,qa,el,grav,nu,delta0)


    !### INTIALISATION DES VARIABLES ###!

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)          :: f
    INTEGER           ,INTENT(IN)                             :: col,N,Mtot
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: H
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: qa
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(INOUT)          :: P,T,BL,Coeff
    DOUBLE PRECISION  ,INTENT(IN)                             :: Dt,Dr 
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: dist,ray
    DOUBLE PRECISION  ,INTENT(IN)                             :: el,grav,nu,delta0

    DOUBLE PRECISION                                          :: h_a,h_b,h_a2,h_b2,h_a3,h_b3,T_a,T_b
    DOUBLE PRECISION                                          :: delta_a,delta_b,delta_a2,delta_b2,delta_a3,delta_b3
    DOUBLE PRECISION                                          :: Ael,Bel,Agrav,Bgrav,phi_a,phi_b
    INTEGER                                                   :: i,err1,algo1


    !### CALCUL DE LA PRESSION ###!

    algo1=2
    CALL  PRESSION_ORDRE4(Mtot,N,Dr,P,H,dist,Coeff,algo1,col,el,delta0)

    !### REMPLISSAGE DE f ###!

    DO i=1,N,1
       IF (i==1) THEN
          Ael=el*(ray(i)/(dist(i)*Dr**2))
          Agrav=grav*(ray(i)/(dist(i)*Dr**2))
          h_a=0.5d0*(H(i+1,col)+H(i,col))
          h_a2=0.5d0*(H(i+1,col)**2+H(i,col)**2)
          h_a3=0.5d0*(H(i+1,col)**3+H(i,col)**3)
          delta_a=0.5d0*(BL(i+1,3)+BL(i,3))
          delta_a2=0.5d0*(BL(i+1,3)**2+BL(i,3)**2)
          delta_a3=0.5d0*(BL(i+1,3)**3+BL(i,3)**3)
          T_a=0.5d0*(T(i,3)+T(i+1,3))
          !T_a=T(i,3)
          phi_a=(nu+(1-nu)*T_a)*h_a3-(2.d0/5.d0)*(1-nu)*T_a*(2*delta_a3-5*delta_a2*h_a+5*delta_a*h_a2)

       ELSEIF (i==N) THEN
          Bel=el*(ray(i-1)/(dist(i)*Dr**2))
          Bgrav=grav*(ray(i-1)/(dist(i)*Dr**2))
          h_b=0.5d0*(H(i,col)+H(i-1,col))
          h_b2=0.5d0*(H(i,col)**2+H(i-1,col)**2)
          h_b3=0.5d0*(H(i,col)**3+H(i-1,col)**3)
          delta_b=0.5d0*(BL(i,3)+BL(i-1,3))
          delta_b2=0.5d0*(BL(i,3)**2+BL(i-1,3)**2)
          delta_b3=0.5d0*(BL(i,3)**3+BL(i-1,3)**3)
          T_b=0.5d0*(T(i,3)+T(i-1,3))
          !T_b=T(i,3)
          phi_b=(nu+(1-nu)*T_b)*h_b3-(2.d0/5.d0)*(1-nu)*T_b*(2*delta_b3-5*delta_b2*h_b+5*delta_b*h_b2)
       ELSE
          Ael=el*(ray(i)/(dist(i)*Dr**2));Bel=el*(ray(i-1)/(dist(i)*Dr**2))
          Agrav=grav*(ray(i)/(dist(i)*Dr**2));Bgrav=grav*(ray(i-1)/(dist(i)*Dr**2))
          h_a=0.5d0*(H(i+1,col)+H(i,col));h_b=0.5d0*(H(i,col)+H(i-1,col))
          h_a2=0.5d0*(H(i+1,col)**2+H(i,col)**2);h_b2=0.5d0*(H(i,col)**2+H(i-1,col)**2)
          h_a3=0.5d0*(H(i+1,col)**3+H(i,col)**3);h_b3=0.5d0*(H(i,col)**3+H(i-1,col)**3)
          delta_a=0.5d0*(BL(i+1,3)+BL(i,3));delta_b=0.5d0*(BL(i,3)+BL(i-1,3))
          delta_a2=0.5d0*(BL(i+1,3)**2+BL(i,3)**2);delta_b2=0.5d0*(BL(i,3)**2+BL(i-1,3)**2)
          delta_a3=0.5d0*(BL(i+1,3)**3+BL(i,3)**3);delta_b3=0.5d0*(BL(i,3)**3+BL(i-1,3)**3)
          T_a=0.5d0*(T(i,3)+T(i+1,3));T_b=0.5d0*(T(i,3)+T(i-1,3))
          !T_a=T(i,3);T_b=T(i-1,3)
          phi_a=(nu+(1-nu)*T_a)*h_a3-(2.d0/5.d0)*(1-nu)*T_a*(2*delta_a3-5*delta_a2*h_a+5*delta_a*h_a2)
          phi_b=(nu+(1-nu)*T_b)*h_b3-(2.d0/5.d0)*(1-nu)*T_b*(2*delta_b3-5*delta_b2*h_b+5*delta_b*h_b2)
       END IF

       IF (i==1) THEN
          f(i)=Ael*phi_a*(P(2,col)-P(1,col))+Agrav*phi_a*(H(2,col)-H(1,col))&
               &+qa(i)
       ELSEIF (i==N) THEN
          f(i)=-Bel*phi_b*(P(i,col)-P(i-1,col))-Bgrav*phi_b*(H(i,col)-H(i-1,col))&
               &+qa(i)
       ELSE
          f(i)=Ael*phi_a*(P(i+1,col)-P(i,col))-Bel*phi_b*(P(i,col)-P(i-1,col))&
               &+Agrav*phi_a*(H(i+1,col)-H(i,col))-Bgrav*phi_b*(H(i,col)-H(i-1,col))&
               &+qa(i)
       END IF
    END DO
 


  END SUBROUTINE THICKNESS

END MODULE THICKNESS_MODULE
