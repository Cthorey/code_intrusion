

MODULE MODULE_OUTPUT

CONTAINS

  SUBROUTINE  OUTPUT(Format,Dt,Mtot,H,T,P,Xi,BL,dist,ray,k,k1,k2,z,compteur,tmps,&
       &Output_Racine,Srr,Stt,delta0,Cas,sample,Format_RV,Format_Backup)

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(INOUT)             :: H,T,P,Xi,BL
    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)             :: dist,ray,Srr,Stt
    DOUBLE PRECISION                 , INTENT(INOUT)             :: tmps,delta0,Dt
    INTEGER                          , INTENT(INOUT)             :: k,k1,k2,z,Mtot,compteur
    CHARACTER(LEN=300)               , INTENT(IN)                :: Format,Output_Racine
    CHARACTER(LEN=300)               , INTENT(INOUT)             :: Format_RV,Format_Backup
    LOGICAL                                                      :: FILE_EXISTS
    INTEGER                          , INTENT(IN)                :: Cas,sample

    INTEGER                                                      :: i,comp_n,comp_o,samp
    CHARACTER(LEN=300)                                           :: Output_Name_F
    CHARACTER(LEN=300)                                           :: Data_file,Format_Data
    DOUBLE PRECISION                                             :: R,hmax,power

   

    SELECT CASE (Cas)

!!! BACKUP FILE
    CASE(0)

       !!!Compteur
       
       IF (tmps == 0D0) THEN
          samp = 1

       ELSE
          power = FLOOR(LOG10(tmps))
          samp = 10**power/Dt

       ENDIF
       
       comp_o = k/samp
       comp_n = (k+1)/samp

       IF (comp_o /= comp_n ) THEN
          compteur = compteur +1
       ENDIF
       
       
       !STOP
       !!! Backup
       IF (compteur == k1) THEN
          WRITE(Output_Name_F,Format_Backup),Output_Racine,'Backup_',compteur,'.dat'
          OPEN(5,file=Output_Name_F,status='replace')
          DO i=1,Mtot,1
             WRITE(5,Format)k,k1,k2,z,compteur,Mtot,tmps,dist(i),ray(i)&
                  &,H(i,1),H(i,2),H(i,3),Xi(i,1),Xi(i,2),Xi(i,3),P(i,1),P(i,2),P(i,3),T(i,1),T(i,2),T(i,3)&
                  &,BL(i,1),BL(i,2),BL(i,3)
          END DO
          CLOSE(5)
          k1=k1+1
       END IF

!!! DATA FILE
    CASE(1)
       IF (compteur==k2) THEN
          WRITE(Data_File,Format_RV)Output_Racine,'RV_',compteur,'.dat'
          OPEN(unit=10,file=Data_File,status='replace')
          Format_Data='(13(D30.24,2X))'
          hmax=H(1,3); R=0.d0
          DO i=1,Mtot,1
             IF (H(i,3)-delta0>0.d0 .OR. R/=0.d0) THEN
                CYCLE
             ELSE
                R=dist(i)
             END IF
          END DO
          DO i=1,Mtot,1
             IF (H(i,3)==delta0) CYCLE
              WRITE(10,Format_Data)tmps,R,dist(i),H(i,3),dist(i)/R,(H(i,3)-delta0)/hmax&
                  &,hmax,P(i,3),T(i,3),BL(i,3),Xi(i,3),Srr(i),Stt(i)
             !WRITE(10,*)tmps,R,dist(i),H(i,3),dist(i)/R,(H(i,3)-delta0)/hmax&
                 ! &,hmax,P(i,3),T(i,3),BL(i,3),Xi(i,3),Srr(i),Stt(i)
          END DO
          CLOSE(10)
          k2=k2+1
       END IF

    END SELECT

END SUBROUTINE OUTPUT

END MODULE MODULE_OUTPUT
