MODULE MODULE_NEWTON_ITERATION_CHALEUR

  USE HEAT_CONSERVATION_MODULE
  USE TEMPERATURE_MODULE
  USE JACOBI_TEMPERATURE_MODULE
  USE NONA_DIAG

CONTAINS

  SUBROUTINE  NEWTON_ITERATION_CHALEUR(H,P,T,Xi,BL,Dt,Dr,dist,ray,Mtot,sigma,nu,Phi,delta0,el,grav,epsilon,k)

    IMPLICIT NONE

    DOUBLE PRECISION  , DIMENSION(:,:), INTENT(INOUT)         :: H,P,T,Xi,BL
    DOUBLE PRECISION  ,                 INTENT(IN)            :: Dt,Dr
    DOUBLE PRECISION  , DIMENSION(:),   INTENT(INOUT)         :: dist,ray
    DOUBLE PRECISION  ,                 INTENT(IN)            :: sigma,nu,Phi,delta0,el,grav,epsilon
    INTEGER,                            INTENT(IN)            :: Mtot,k

    INTEGER                                                   :: z,i
    DOUBLE PRECISION                                          :: N2,N2t

    Xi(:,4)=0.d0
    N2=20;N2t=20;z=0
 

    ITERATION_TEMPERATURE: DO 
       CALL HEAT_CONSERVATION(Xi,H,P,T,BL,Dt,Dr,dist,ray,Mtot,sigma,nu,Phi,delta0,el,grav)
       DO i=1,Mtot,1
          IF (Xi(i,2)>1D-10) THEN
             Xi(i,6)=(Xi(i,3)-Xi(i,2))/Xi(i,2)
          ELSE
             Xi(i,6)=Xi(i,3)
          END IF
       END DO
       N2=ABS(MAXVAL((Xi(:,6))));z=z+1
       !N2=ABS(MAXVAL((Xi(:,3)-Xi(:,2))));z=z+1
       !print*,'Iteration_Temperature',z,N2,N2t
       IF ( N2>N2t .OR. z>20000) THEN
        !  PRINT*,'Erreur_Ite_Temp',N2,N2t
         ! STOP
       END IF
       IF (N2<epsilon) EXIT
       Xi(:,2)=Xi(:,3) ; Xi(:,3) = 0.d0 
       BL(:,2)=BL(:,3);T(:,2)=T(:,3)
       N2t=N2
    END DO ITERATION_TEMPERATURE


  END SUBROUTINE NEWTON_ITERATION_CHALEUR

END MODULE MODULE_NEWTON_ITERATION_CHALEUR
