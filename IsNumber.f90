MODULE Is_NAN_OR_Infty

CONTAINS

  SUBROUTINE  IsNanInfty(a,b)
    
    DOUBLE PRECISION    ,INTENT(IN)         :: a
    INTEGER             ,INTENT(INOUT)      :: b

    b=0
    IF (a>1D170 .OR. a/=a) THEN
       b=1
    END IF
  END SUBROUTINE IsNanInfty

END MODULE Is_NAN_OR_Infty
