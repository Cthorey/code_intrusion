MODULE JACOBI_THICKNESS_MODULE

  USE PRESSURE

CONTAINS

  SUBROUTINE  JACOBI_THICKNESS(a,b,c,d,e,f,g,k,l,N,H,P,T,BL,Coeff,Dt,Dr,dist,ray,el,grav,nu,Mtot,delta0)

    !### INTIALISATION DES VARIABLES ###!

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)          :: a,b,c,d,e,f,g,k,l
    INTEGER           ,INTENT(IN)                             :: N,Mtot
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: H,T,BL
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(INOUT)          :: P,Coeff
    DOUBLE PRECISION  ,INTENT(IN)                             :: Dt,Dr 
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: dist,ray
    DOUBLE PRECISION  ,INTENT(IN)                             :: el,grav,nu,delta0

    DOUBLE PRECISION                                          :: Ael,Bel,Agrav,Bgrav,h_a,h_b,h_a2,h_b2,h_a3,h_b3,T_a,T_b
    DOUBLE PRECISION                                          :: delta_a,delta_b,delta_a2,delta_b2,delta_a3,delta_b3
    DOUBLE PRECISION                                          :: phi_a,phi_b,dphib_dhi,dphib_dhi1,dphia_dhi,dphia_dhi1
    DOUBLE PRECISION                                          :: H1,H2,P1,P2
    INTEGER                                                   :: i,col,algo1,err1
    DOUBLE PRECISION  ,DIMENSION(:), ALLOCATABLE              :: alpha,beta,gamma,lambda,kappa,delta,epsilonn


    !! REMPLISSAGE DE LA MATRICE JACOBIENNE 
    ALLOCATE(alpha(1:N),beta(1:N),gamma(1:N),lambda(1:N),stat=err1)
    ALLOCATE(kappa(1:N),delta(1:N),epsilonn(1:N),stat=err1)
    IF (err1>1) THEN !On teste si nos tableau sont bien alloués
       PRINT*,"Err1 MAIN dans l''allcation" ;STOP
    END IF

    algo1=2;col=2
    CALL  PRESSION_ORDRE4(Mtot,N,Dr,P,H,dist,Coeff,algo1,col,el,delta0)

    alpha=Coeff(:,1)
    beta=Coeff(:,2)
    gamma=Coeff(:,3)
    lambda=Coeff(:,4)
    kappa=Coeff(:,5)
    delta=Coeff(:,6)
    epsilonn=Coeff(:,7)

    !! REMPLISSAGE DE LA MATRICE JACOBIENNE ###!

    DO i=1,N,1

       IF1: IF (i==1) THEN
          Ael=el*(ray(i)/(dist(i)*Dr**2))
          Agrav=grav*(ray(i)/(dist(i)*Dr**2))
          h_a=0.5d0*(H(i+1,col)+H(i,col))
          h_a2=0.5d0*(H(i+1,col)**2+H(i,col)**2)
          h_a3=0.5d0*(H(i+1,col)**3+H(i,col)**3)
          delta_a=0.5d0*(BL(i+1,3)+BL(i,3))
          delta_a2=0.5d0*(BL(i+1,3)**2+BL(i,3)**2)
          delta_a3=0.5d0*(BL(i+1,3)**3+BL(i,3)**3)
          T_a=0.5d0*(T(i,3)+T(i+1,3))
          phi_a=(nu+(1-nu)*T_a)*h_a3-(2.d0/5.d0)*(1-nu)*T_a*(2*delta_a3-5*delta_a2*h_a+5*delta_a*h_a2)
          dphia_dhi1=(3.d0/2.d0)*(nu+(1-nu)*T_a)*H(i+1,col)**2-(1-nu)*T_a*(-delta_a2+delta_a*H(i+1,col)) ! d(phi_i+1/2)/d(h_i+1)
          dphia_dhi=(3.d0/2.d0)*(nu+(1-nu)*T_a)*H(i,col)**2-(1-nu)*T_a*(-delta_a2+delta_a*H(i,col))     ! d(phi_i+1/2)/d(h_i)
          P1=P(i+1,2)-P(i,2); P2=P(i,2)-P(i-1,2)
          H1=H(i+1,2)-H(i,2); H2=H(i,2)-H(i-1,2)

       ELSEIF (i==N) THEN
          Bel=el*(ray(i-1)/(dist(i)*Dr**2))
          Bgrav=grav*(ray(i-1)/(dist(i)*Dr**2))
          h_b=0.5d0*(H(i,col)+H(i-1,col))
          h_b2=0.5d0*(H(i,col)**2+H(i-1,col)**2)
          h_b3=0.5d0*(H(i,col)**3+H(i-1,col)**3)
          delta_b=0.5d0*(BL(i,3)+BL(i-1,3))
          delta_b2=0.5d0*(BL(i,3)**2+BL(i-1,3)**2)
          delta_b3=0.5d0*(BL(i,3)**3+BL(i-1,3)**3)
          T_b=0.5d0*(T(i,3)+T(i-1,3))
          phi_b=(nu+(1-nu)*T_b)*h_b3-(2.d0/5.d0)*(1-nu)*T_b*(2*delta_b3-5*delta_b2*h_b+5*delta_b*h_b2)
          dphib_dhi=(3.d0/2.d0)*(nu+(1-nu)*T_b)*H(i,col)**2-(1-nu)*T_b*(-delta_b2+delta_b*H(i,col))     ! d(phi_i-1/2)/d(h_i)
          dphib_dhi1=(3.d0/2.d0)*(nu+(1-nu)*T_b)*H(i-1,col)**2-(1-nu)*T_b*(-delta_b2+delta_b*H(i-1,col)) ! d(phi_i-1/2)/d(h_i-1)
          P2=P(i,2)-P(i-1,2)
          H2=H(i,2)-H(i-1,2)

       ELSE
          Ael=el*(ray(i)/(dist(i)*Dr**2));Bel=el*(ray(i-1)/(dist(i)*Dr**2))
          Agrav=grav*(ray(i)/(dist(i)*Dr**2));Bgrav=grav*(ray(i-1)/(dist(i)*Dr**2))
          h_a=0.5d0*(H(i+1,col)+H(i,col));h_b=0.5d0*(H(i,col)+H(i-1,col))
          h_a2=0.5d0*(H(i+1,col)**2+H(i,col)**2);h_b2=0.5d0*(H(i,col)**2+H(i-1,col)**2)
          h_a3=0.5d0*(H(i+1,col)**3+H(i,col)**3);h_b3=0.5d0*(H(i,col)**3+H(i-1,col)**3)
          delta_a=0.5d0*(BL(i+1,3)+BL(i,3));delta_b=0.5d0*(BL(i,3)+BL(i-1,3))
          delta_a2=0.5d0*(BL(i+1,3)**2+BL(i,3)**2);delta_b2=0.5d0*(BL(i,3)**2+BL(i-1,3)**2)
          delta_a3=0.5d0*(BL(i+1,3)**3+BL(i,3)**3);delta_b3=0.5d0*(BL(i,3)**3+BL(i-1,3)**3)
          T_a=0.5d0*(T(i,3)+T(i+1,3));T_b=0.5d0*(T(i,3)+T(i-1,3))
          phi_a=(nu+(1-nu)*T_a)*h_a3-(2.d0/5.d0)*(1-nu)*T_a*(2*delta_a3-5*delta_a2*h_a+5*delta_a*h_a2)
          phi_b=(nu+(1-nu)*T_b)*h_b3-(2.d0/5.d0)*(1-nu)*T_b*(2*delta_b3-5*delta_b2*h_b+5*delta_b*h_b2)
          dphia_dhi1=(3.d0/2.d0)*(nu+(1-nu)*T_a)*H(i+1,col)**2-(1-nu)*T_a*(-delta_a2+delta_a*H(i+1,col)) ! d(phi_i+1/2)/d(h_i+1)
          dphia_dhi=(3.d0/2.d0)*(nu+(1-nu)*T_a)*H(i,col)**2-(1-nu)*T_a*(-delta_a2+delta_a*H(i,col))     ! d(phi_i+1/2)/d(h_i)
          dphib_dhi=(3.d0/2.d0)*(nu+(1-nu)*T_b)*H(i,col)**2-(1-nu)*T_b*(-delta_b2+delta_b*H(i,col))     ! d(phi_i-1/2)/d(h_i)
          dphib_dhi1=(3.d0/2.d0)*(nu+(1-nu)*T_b)*H(i-1,col)**2-(1-nu)*T_b*(-delta_b2+delta_b*H(i-1,col)) ! d(phi_i-1/2)/d(h_i-1)
          P1=P(i+1,2)-P(i,2); P2=P(i,2)-P(i-1,2)
          H1=H(i+1,2)-H(i,2); H2=H(i,2)-H(i-1,2)

       END IF IF1

       IF2: IF (i==1) THEN
          a(i)=0;b(i)=0;c(i)=0;d(i)=0
          e(i)=Ael*dphia_dhi*P1+Ael*phi_a*((gamma(i+1)+beta(i+1))-(lambda(i)+gamma(i)))&
               &+Agrav*(dphia_dhi*H1-phi_a)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*((lambda(i+1)+alpha(i+1))-(kappa(i)+beta(i)))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-(delta(i)+alpha(i)))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))
          l(i)=Ael*phi_a*epsilonn(i+1)

       ELSEIF (i==2) THEN
          a(i)=0;b(i)=0;c(i)=0
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*((beta(i+1)+alpha(i+1))-(gamma(i)+beta(i)))&
               &-Bel*phi_b*((gamma(i)+beta(i))-(lambda(i-1)+gamma(i-1)))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-(lambda(i)+alpha(i)))&
               &-Bel*phi_b*((lambda(i)+alpha(i))-(kappa(i-1)+beta(i-1)))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))&
               &-Bel*phi_b*(kappa(i)-(delta(i-1)+alpha(i-1)))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))-Bel*phi_b*epsilonn(i)
          l(i)=Ael*phi_a*epsilonn(i+1)
       ELSEIF (i==3) THEN
          a(i)=0;b(i)=0
          c(i)=Ael*phi_a*(alpha(i+1)-(beta(i)+alpha(i)))-Bel*phi_b*((beta(i)+alpha(i))-(gamma(i-1)+beta(i-1)))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-(lambda(i-1)+alpha(i-1)))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))-Bel*phi_b*epsilonn(i)
          l(i)=Ael*phi_a*epsilonn(i+1)
       ELSEIF (i==4) THEN
          a(i)=0
          b(i)=-Ael*phi_a*alpha(i)-Bel*phi_b*(alpha(i)-(beta(i-1)+alpha(i-1)))
          c(i)=Ael*phi_a*(alpha(i+1)-beta(i))-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))-Bel*phi_b*epsilonn(i)
          l(i)=Ael*phi_a*epsilonn(i+1)

       ELSEIF (i==N-3) THEN
          a(i)=Bel*phi_b*alpha(i-1)
          b(i)=-Ael*phi_a*alpha(i)-Bel*phi_b*(alpha(i)-beta(i-1))
          c(i)=Ael*phi_a*(alpha(i+1)-beta(i))-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))-Bel*phi_b*epsilonn(i)
          l(i)=0
       ELSEIF (i==N-2) THEN
          a(i)=Bel*phi_b*alpha(i-1)
          b(i)=-Ael*phi_a*alpha(i)-Bel*phi_b*(alpha(i)-beta(i-1))
          c(i)=Ael*phi_a*(alpha(i+1)-beta(i))-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=0;l(i)=0
       ELSEIF (i==N-1) THEN
          a(i)=Bel*phi_b*alpha(i-1)
          b(i)=-Ael*phi_a*alpha(i)-Bel*phi_b*(alpha(i)-beta(i-1))
          c(i)=Ael*phi_a*(alpha(i+1)-beta(i))-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=0.d0;k(i)=0.d0;l(i)=0.d0

       ELSEIF (i==N) THEN
          a(i)=Bel*phi_b*alpha(i-1)
          b(i)=-Bel*phi_b*(alpha(i)-beta(i-1))
          c(i)=-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=-Bel*dphib_dhi*P2-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=0.d0;g(i)=0.d0;k(i)=0.d0;l(i)=0.d0
       ELSE
          a(i)=Bel*phi_b*alpha(i-1)
          b(i)=-Ael*phi_a*alpha(i)-Bel*phi_b*(alpha(i)-beta(i-1))
          c(i)=Ael*phi_a*(alpha(i+1)-beta(i))-Bel*phi_b*(beta(i)-gamma(i-1))
          d(i)=-Bel*dphib_dhi1*P2+Ael*phi_a*(beta(i+1)-gamma(i))-Bel*phi_b*(gamma(i)-lambda(i-1))&
               &+Bgrav*(-dphib_dhi1*H2+phi_b)
          e(i)=Ael*dphia_dhi*P1-Bel*dphib_dhi*P2+Ael*phi_a*(gamma(i+1)-lambda(i))-Bel*phi_b*(lambda(i)-kappa(i-1))&
               &+Agrav*(dphia_dhi*H1-phi_a)-Bgrav*(dphib_dhi*H2+phi_b)
          f(i)=Ael*dphia_dhi1*P1+Ael*phi_a*(lambda(i+1)-kappa(i))-Bel*phi_b*(kappa(i)-delta(i-1))&
               &+Agrav*(dphia_dhi1*H1+phi_a)
          g(i)=Ael*phi_a*(kappa(i+1)-delta(i))-Bel*phi_b*(delta(i)-epsilonn(i-1))
          k(i)=Ael*phi_a*(delta(i+1)-epsilonn(i))-Bel*phi_b*epsilonn(i)
          l(i)=Ael*phi_a*epsilonn(i+1)
       END IF IF2

    END DO


    DEALLOCATE(alpha,beta,gamma,lambda,kappa,delta,epsilonn)

  END SUBROUTINE JACOBI_THICKNESS
END MODULE JACOBI_THICKNESS_MODULE
