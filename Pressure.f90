MODULE PRESSURE
 

CONTAINS


SUBROUTINE  PRESSION_ORDRE4(Mtot,N,Dr,P,H,dist,Coeff,algo1,col,el,delta0)

IMPLICIT NONE

INTEGER         , INTENT(IN)                              :: Mtot,N,col,algo1
DOUBLE PRECISION, INTENT(IN)                              :: Dr,el,delta0
DOUBLE PRECISION, DIMENSION(:),   INTENT(IN)              :: dist
DOUBLE PRECISION, DIMENSION(:,:), INTENT(INOUT)           :: Coeff
DOUBLE PRECISION, DIMENSION(:,:), INTENT(IN)              :: H
DOUBLE PRECISION, DIMENSION(:,:), INTENT(INOUT)           :: P

INTEGER                                                   :: i,err1
DOUBLE PRECISION                                          :: p1,p2,p3,p4




                        !!!!!!!!!!!!!!!!!!!! SCHEMA CENTRE DORDRE 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     
SELECT CASE(algo1)
   CASE(1)

     DO i=1,N,1
        p1=1.d0/dist(i)**3
        p2=-1.d0/(dist(i)**2)
        p3=2.d0/dist(i)
        p4=1.d0
        
        Coeff(i,1)=(1.d0/(24.d0*Dr**4))*(-4.d0*p4+3.d0*p3*Dr)
        Coeff(i,2)=(1.d0/(24.d0*Dr**4))*(48.d0*p4-24.d0*p3*Dr-2.d0*p2*Dr**2+2.d0*p1*Dr**3)
        Coeff(i,3)=(1.d0/(24.d0*Dr**4))*(-156.d0*p4+39.d0*p3*Dr+32.d0*p2*Dr**2-16.d0*p1*Dr**3)
        Coeff(i,4)=(1.d0/(24.d0*Dr**4))*(224.d0*p4-60.d0*p2*Dr**2)
        Coeff(i,5)=(1.d0/(24.d0*Dr**4))*(-156.d0*p4-39*p3*Dr+32.d0*p2*Dr**2+16.d0*p1*Dr**3)
        Coeff(i,6)=(1.d0/(24.d0*Dr**4))*(48.d0*p4+24.d0*p3*Dr-2.d0*p2*Dr**2-2.d0*p1*Dr**3)
        Coeff(i,7)=(1.d0/(24.d0*Dr**4))*(-4.d0*p4-3.d0*p3*Dr)
             
         END DO

    CASE(2)
         DO i=1,Mtot,1
            IF (i==1) THEN
               P(i,col)=el*(Coeff(i,1)*H(3,col)+Coeff(i,2)*H(2,col)+Coeff(i,3)*H(1,col)+Coeff(i,4)*H(1,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*H(i+2,col)+Coeff(i,7)*H(i+3,col))
            ELSEIF (i==2) THEN
               P(i,col)=el*(Coeff(i,1)*H(2,col)+Coeff(i,2)*H(1,col)+Coeff(i,3)*H(1,col)+Coeff(i,4)*H(2,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*H(i+2,col)+Coeff(i,7)*H(i+3,col))
            ELSEIF (i==3) THEN
               P(i,col)=el*(Coeff(i,1)*H(1,col)+Coeff(i,2)*H(1,col)+Coeff(i,3)*H(2,col)+Coeff(i,4)*H(3,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*H(i+2,col)+Coeff(i,7)*H(i+3,col))
            ELSEIF (i==N-2) THEN
               P(i,col)=Coeff(i,1)*H(i-3,col)+Coeff(i,2)*H(i-2,col)+Coeff(i,3)*H(i-1,col)+Coeff(i,4)*H(i,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*H(i+2,col)+Coeff(i,7)*delta0
            ELSEIF (i==N-1) THEN
               P(i,col)=Coeff(i,1)*H(i-3,col)+Coeff(i,2)*H(i-2,col)+Coeff(i,3)*H(i-1,col)+Coeff(i,4)*H(i,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*delta0+Coeff(i,7)*delta0
            ELSEIF (i==N) THEN
               P(i,col)=Coeff(i,1)*H(i-3,col)+Coeff(i,2)*H(i-2,col)+Coeff(i,3)*H(i-1,col)+Coeff(i,4)*H(i,col)+Coeff(i,5)*delta0&
                    &+Coeff(i,6)*delta0+Coeff(i,7)*delta0
            ELSE
               P(i,col)=Coeff(i,1)*H(i-3,col)+Coeff(i,2)*H(i-2,col)+Coeff(i,3)*H(i-1,col)+Coeff(i,4)*H(i,col)+Coeff(i,5)*H(i+1,col)&
                    &+Coeff(i,6)*H(i+2,col)+Coeff(i,7)*H(i+3,col)
            END IF
            IF (el==0) THEN
               P(i,col)=0
            END IF
         END DO

      END SELECT

    END SUBROUTINE PRESSION_ORDRE4
       
  END MODULE PRESSURE
