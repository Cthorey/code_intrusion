MODULE JACOBI_TEMPERATURE_MODULE

CONTAINS

  SUBROUTINE  JACOBI_TEMPERATURE(a,b,c,d,e,f,g,k,l,N,H,T,P,BL,Dr,dist,ray,nu,Phi,qa,delta0,el,grav)

    !### INTIALISATION DES VARIABLES ###!

    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)          :: a,b,c,d,e,f,g,k,l
    INTEGER           ,INTENT(IN)                             :: N
    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(IN)             :: H,T,P,BL
    DOUBLE PRECISION  ,INTENT(IN)                             :: Dr 
    DOUBLE PRECISION  ,DIMENSION(:), INTENT(IN)               :: qa,dist,ray
    DOUBLE PRECISION  ,INTENT(IN)                             :: nu,Phi,delta0,el,grav

    DOUBLE PRECISION                                          :: Ai,Bi
    DOUBLE PRECISION                                          :: h_a,h_a2,delta_a,delta_a2,eta_a,zeta_a,Omega_a,T_a
    DOUBLE PRECISION                                          :: h_b,h_b2,delta_b,delta_b2,eta_b,zeta_b,Omega_b,T_b
    INTEGER                                                   :: i,col


    !### REMPLISSAGE DE LA MATRICE JACOBIENNE ###!

    col=2

    DO i=1,N,1
       IF1: IF (i/=1 .AND. i/=N) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3));h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2);h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col));delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2);delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_a=(grav*(H(i+1,3)-H(i,3))+el*(P(i+1,3)-P(i,3)))/Dr
          eta_b=(grav*(H(i,3)-H(i-1,3))+el*(P(i,3)-P(i-1,3)))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col));T_b=0.5d0*(T(i,col)+T(i-1,col))
          zeta_a=h_a*(sqrt(5.d0)*h_a+sqrt(2.d0)*delta_a)/(sqrt(2.d0))
          zeta_b=h_b*(sqrt(5.d0)*h_b+sqrt(2.d0)*delta_b)/(sqrt(2.d0))
          Omega_a=eta_a*((3.d0)/(5.d0)*nu*(zeta_a-delta_a2)+(1.d0-nu)*T_a*(3*h_a2+delta_a2))
          Omega_b=eta_b*((3.d0)/(5.d0)*nu*(zeta_b-delta_b2)+(1.d0-nu)*T_b*(3*h_b2+delta_b2))
       ELSE IF (i==N) THEN
          Bi=(ray(i-1)/(dist(i)*Dr))
          h_b=0.5d0*(H(i,3)+H(i-1,3))
          h_b2=0.5d0*(H(i,3)**2+H(i-1,3)**2)
          delta_b=0.5d0*(BL(i,col)+BL(i-1,col))
          delta_b2=0.5d0*(BL(i,col)**2+BL(i-1,col)**2)
          eta_b=(grav*(H(i,3)-H(i-1,3))+el*(P(i,3)-P(i-1,3)))/Dr
          T_b=0.5d0*(T(i,col)+T(i-1,col))
          zeta_b=h_b*(sqrt(5.d0)*h_b+sqrt(2.d0)*delta_b)/(sqrt(2.d0))
          Omega_b=eta_b*((3.d0)/(5.d0)*nu*(zeta_b-delta_b2)+(1.d0-nu)*T_b*(3*h_b2+delta_b2))
       ELSE IF (i==1) THEN
          Ai=(ray(i)/(dist(i)*Dr))
          h_a=0.5d0*(H(i+1,3)+H(i,3))
          h_a2=0.5d0*(H(i+1,3)**2+H(i,3)**2)
          delta_a=0.5d0*(BL(i+1,col)+BL(i,col))
          delta_a2=0.5d0*(BL(i+1,col)**2+BL(i,col)**2)
          eta_a=(grav*(H(i+1,3)-H(i,3))+el*(P(i+1,3)-P(i,3)))/Dr
          T_a=0.5d0*(T(i+1,col)+T(i,col))
          zeta_a=h_a*(sqrt(5.d0)*h_a+sqrt(2.d0)*delta_a)/(sqrt(2.d0))
          Omega_a=eta_a*((3.d0)/(5.d0)*nu*(zeta_a-delta_a2)+(1.d0-nu)*T_a*(3*h_a2+delta_a2))
       END IF IF1

       IF2:IF (i==1) THEN
          a(i)=0; b(i)=0; c(i)=0;d(i)=0
          e(i)=Ai*Omega_a
          f(i)=0;g(i)=0;k(i)=0;l(i)=0
       ELSEIF (i==N) THEN
          a(i)=0; b(i)=0; c(i)=0;
          d(i)=-Bi*Omega_b;
          e(i)=0.d0;
          f(i)=0;g(i)=0;k(i)=0;l(i)=0  
       ELSE
          a(i)=0.d0;b(i)=0.d0;c(i)=0.d0
          d(i)=-Bi*Omega_b
          e(i)=Ai*Omega_a
          f(i)=0;g(i)=0;k(i)=0;l(i)=0
       END IF IF2
    END DO

  END SUBROUTINE JACOBI_TEMPERATURE
END MODULE JACOBI_TEMPERATURE_MODULE
