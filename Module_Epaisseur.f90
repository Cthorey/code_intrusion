MODULE MODULE_NEWTON_ITERATION_EPAISSEUR

  USE MASS_CONSERVATION_MODULE
  USE THICKNESS_MODULE
  USE JACOBI_THICKNESS_MODULE
  USE NONA_DIAG
  USE PRESSURE

CONTAINS

  SUBROUTINE  NEWTON_ITERATION_EPAISSEUR(H,P,T,BL,Dt,Dr,Mtot,dist,ray,el,grav,sigma,nu,delta0,epsilon)

    IMPLICIT NONE

    DOUBLE PRECISION  , DIMENSION(:,:) ,INTENT(INOUT)         :: H,P,T,BL
    DOUBLE PRECISION  ,                 INTENT(IN)            :: Dt,Dr
    DOUBLE PRECISION  , DIMENSION(:)   ,INTENT(IN)            :: dist,ray
    DOUBLE PRECISION  ,                 INTENT(IN)            :: el,sigma,grav,nu,delta0,epsilon
    INTEGER                            ,INTENT(IN)            :: Mtot

    INTEGER                                                   :: z,i
    DOUBLE PRECISION                                          :: N1,N1t


    H(:,4)=0.d0
    N1=20;N1t=20;z=0

    ITERATION_THICKNESS: DO 
       CALL  MASS_CONSERVATION(H,P,T,BL,Dt,Dr,Mtot,dist,ray,el,grav,sigma,nu,delta0)
       N1=ABS(MAXVAL((H(:,4)/H(:,2))));z=z+1
       !PRINT*,'Iteration_Epais',N1,N1t
       IF ( N1>N1t .OR. z>20000) THEN
        !  PRINT*,'Erreur_Ite_Epais',N1,N1t
          STOP
       END IF
       IF (N1<epsilon) EXIT
       H(:,2) = H(:,3); H(:,3) = 0.d0
       N1t = N1
    END DO ITERATION_THICKNESS

  END SUBROUTINE NEWTON_ITERATION_EPAISSEUR

END MODULE MODULE_NEWTON_ITERATION_EPAISSEUR
