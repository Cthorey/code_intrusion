MODULE NONA_DIAG

CONTAINS

  SUBROUTINE  NONA_DIAGO(N,Hm,a,b,c,d,e,f,g,k,l,S)

  IMPLICIT NONE
  
  INTEGER         , INTENT(IN)                           :: N
  INTEGER                                                :: i
  INTEGER                                                :: err3,err4
  DOUBLE PRECISION, DIMENSION(:), INTENT(IN)             :: a,b,c,d,e,f,g,k,l,S
  DOUBLE PRECISION, DIMENSION(:),INTENT(INOUT)           :: Hm
  DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE            :: zeta,alpha,beta,mu,xi,lambda,eta,omega,gamma
  
  


      AllOCATE(zeta(1:N),alpha(1:N),beta(1:N),mu(1:N),xi(1:N),stat=err3)
      ALLOCATE(lambda(1:N),eta(1:N),omega(1:N),gamma(1:N),stat=err4)

      IF (err3>1 .OR. err4>1) THEN
         PRINT*, 'Erreur d''allocation dans vecteur P,Q'; STOP
      END IF
       
                
      zeta(1)=b(1)
      alpha(1)=c(1)
      beta(1)=d(1)
      mu(1)=e(1)
      xi(1)=f(1)/mu(1)
      lambda(1)=g(1)/mu(1)
      eta(1)=k(1)/mu(1)
      omega(1)=l(1)/mu(1)
      gamma(1)=S(1)/mu(1)

      zeta(2)=b(2)
      alpha(2)=c(2)
      beta(2)=d(2)
      mu(2)=e(2)-xi(1)*beta(2)
      xi(2)=(f(2)-lambda(1)*beta(2))/mu(2)
      lambda(2)=(g(2)-eta(1)*beta(2))/mu(2)
      eta(2)=(k(2)-omega(1)*beta(2))/mu(2)
      omega(2)=l(2)/mu(2)
      gamma(2)=(S(2)-beta(2)*gamma(1))/mu(2)

      zeta(3)=b(3)
      alpha(3)=c(3)
      beta(3)=d(3)-xi(1)*alpha(3)
      mu(3)=e(3)-lambda(1)*alpha(3)-xi(2)*beta(3)
      xi(3)=(f(3)-eta(1)*alpha(3)-lambda(2)*beta(3))/mu(3)
      lambda(3)=(g(3)-omega(1)*alpha(3)-eta(2)*beta(3))/mu(3)
      eta(3)=(k(3)-omega(2)*beta(3))/mu(3)
      omega(3)=l(3)/mu(3)
      gamma(3)=(S(3)-alpha(3)*gamma(1)-beta(3)*gamma(2))/mu(3)

      zeta(4)=b(4)
      alpha(4)=c(4)-xi(1)*zeta(4)
      beta(4)=d(4)-lambda(1)*zeta(4)-xi(2)*alpha(4)
      mu(4)=e(4)-eta(1)*zeta(4)-lambda(2)*alpha(4)-xi(3)*beta(4)
      xi(4)=(f(4)-omega(1)*zeta(4)-eta(2)*alpha(4)-lambda(3)*beta(4))/mu(4)
      lambda(4)=(g(4)-omega(2)*alpha(4)-eta(3)*beta(4))/mu(4)
      eta(4)=(k(4)-omega(3)*beta(4))/mu(4)
      omega(4)=l(4)/mu(4)
      gamma(4)=(S(4)-zeta(4)*gamma(1)-alpha(4)*gamma(2)-beta(4)*gamma(3))/mu(4)

      DO i=5,N
                   
         zeta(i)=b(i)-a(i)*xi(i-4)
         alpha(i)=c(i)-a(i)*lambda(i-4)-xi(i-3)*zeta(i)
         beta(i)=d(i)-a(i)*eta(i-4)-lambda(i-3)*zeta(i)-alpha(i)*xi(i-2)
         mu(i)=e(i)-a(i)*omega(i-4)-zeta(i)*eta(i-3)-lambda(i-2)*alpha(i)-beta(i)*xi(i-1)
         xi(i)=(f(i)-omega(i-3)*zeta(i)-eta(i-2)*alpha(i)-lambda(i-1)*beta(i))/mu(i)
         lambda(i)=(g(i)-alpha(i)*omega(i-2)-eta(i-1)*beta(i))/mu(i)
         eta(i)=(k(i)-omega(i-1)*beta(i))/mu(i)
         omega(i)=l(i)/mu(i)
         gamma(i)=(S(i)-a(i)*gamma(i-4)-zeta(i)*gamma(i-3)-alpha(i)*gamma(i-2)-beta(i)*gamma(i-1))/mu(i)
    
      END DO
              
      Hm(N)=gamma(N)
      Hm(N-1)=gamma(N-1)-xi(N-1)*Hm(N)
      Hm(N-2)=gamma(N-2)-lambda(N-2)*Hm(N)-xi(N-2)*Hm(N-1)
      Hm(N-3)=gamma(N-3)-eta(N-3)*Hm(N)-lambda(N-3)*Hm(N-1)-xi(N-3)*Hm(N-2)
      DO i=N-4,1,-1
         Hm(i)=gamma(i)-xi(i)*Hm(i+1)-lambda(i)*Hm(i+2)-eta(i)*Hm(i+3)-omega(i)*Hm(i+4)
      END DO
      
      DEALLOCATE(zeta,alpha,beta,mu,xi,lambda,eta,omega,gamma)

    END SUBROUTINE NONA_DIAGO

  END MODULE NONA_DIAG
