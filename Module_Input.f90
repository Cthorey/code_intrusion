MODULE MODULE_INPUT

CONTAINS

  SUBROUTINE  INPUT(Format,Mtot,H,T,P,Xi,BL,dist,ray,k,k1,k2,z,tmps,Init,compteur,Input_Name)
    
    IMPLICIT NONE

    DOUBLE PRECISION  ,DIMENSION(:,:), INTENT(INOUT)             :: H,T,P,Xi,BL
    DOUBLE PRECISION  ,DIMENSION(:)  , INTENT(INOUT)             :: dist,ray
    DOUBLE PRECISION                 , INTENT(INOUT)             :: tmps
    INTEGER                          , INTENT(INOUT)             :: k,k1,k2,z,Mtot,compteur
    CHARACTER(LEN=300)               , INTENT(IN)                :: Format,Input_Name
    INTEGER                          , INTENT(IN)                :: Init
    LOGICAL                                                      :: FILE_EXISTS

    INTEGER                                                      :: i



    SELECT CASE (Init)
    CASE(0)
    CASE(1)
       INQUIRE(FILE=Input_Name, EXIST=FILE_EXISTS)  
       IF (FILE_EXISTS) THEN
          OPEN(1,file=Input_Name)
          DO i=1,Mtot,1
             READ(1,Format)k,k1,k2,z,compteur,Mtot,tmps,dist(i),ray(i)&
                  &,H(i,1),H(i,2),H(i,3),Xi(i,1),Xi(i,2),Xi(i,3),P(i,1),P(i,2),P(i,3),T(i,1),T(i,2),T(i,3)&
                  &,BL(i,1),BL(i,2),BL(i,3)
          END DO
          CLOSE(1)
       ELSEIF( .NOT. FILE_EXISTS) THEN
          PRINT*,'ERREUR: PAS DE FICHIER INPUT. CHOISIR LE MODE INIT=0 AND TRY AGAIN'
          STOP
       ENDIF
    END SELECT

  END SUBROUTINE INPUT

END MODULE MODULE_INPUT
